
const express = require('express')
const cors = require('cors')
const app = express()
const port = 3000

app.use(cors())
app.use(express.json());




app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

////////////////////////////////////////////////////////////////////////

app.post('/users/chang', async(req, res) => {
  const pass =req.body.pass;
  const client = new MongoClient(uri);
  await client.connect();
  console.log(pass)
  var crypto = require('crypto');

  var mykey = crypto.createCipher('aes-128-cbc', 'test1234');
  var mystr = mykey.update(pass, 'utf8', 'hex')
  mystr += mykey.final('hex');
  console.log(mystr);

  await client.close();
  res.status(200).send({
    "status": true,
    "message": mystr,
  });
})


const { MongoClient } = require("mongodb");
const uri = "mongodb://test:test1234@localhost:27017";

app.post('/users/create', async(req, res) => {
  const user = req.body;
  const userName = user.userName;
  const client = new MongoClient(uri);
  await client.connect();
  let check = false
  const userFind = await client.db('mydb')?.collection('users')?.findOne({"userName": userName});
  console.log(userFind)
  if(userFind == null){
    check = false
  } else{
    if(userFind.userName == userName ){
      check = true
    }else {
      check = false
    }
  }
  if(check){

    res.status(200).send({
      "status": false,
      "message": "คนมีใช้งานแล้ว",
      "user": user
    });
  }else{
    console.log(user.passWord)
    console.log(user.passWord[0])
    var crypto = require('crypto');

    var mykey = crypto.createDecipher('aes-128-cbc', 'test1234');
    var passchang = mykey.update(user.passWord[0], 'hex', 'utf8')
    passchang += mykey.final('utf8');

    console.log(passchang);
    let passCom = []
    passCom.push(passchang)
    
    await client.db('mydb').collection('users').insertOne({
      userName: user.userName,
      passWord:passCom,
      fname: user.fname,
      lname: user.lname,
      img: user.img
    });
    await client.close();
    res.status(200).send({
      "status": true,
      "message": "สำเร็จ",
      "user": user
    });
  }

  
})

app.post('/users/update', async(req, res) => {
  let PassWordSet = []
  let Station = false
  const user = req.body;
  const userName = user.userName;
  let find = false
  let text
  const client = new MongoClient(uri);
  await client.connect();
  const userFind = await client.db('mydb').collection('users').findOne({"userName": userName});
  userFind.passWord.some(item =>{
    if(item != user.passWord && item != null){
      find = false
    }else{
      find = true
      return true;
    }
  })
  if(find == false && userFind.passWord.length <= 4){
    PassWordSet = userFind.passWord
    PassWordSet.push(user.passWord)
    console.log("--------------------1");
    console.log(PassWordSet);
  }else if (find == false && userFind.passWord.length >= 4){
    PassWordSet = userFind.passWord
    PassWordSet.splice(0,1)
    PassWordSet.push(user.passWord)
    console.log("--------------------2");
    console.log(PassWordSet);
  }else if ( userFind.passWord.length == 4){
    PassWordSet = userFind.passWord
    PassWordSet.push(user.passWord)
    console.log("--------------------3");
    console.log(PassWordSet);
  }else {
    PassWordSet = userFind.passWord
    console.log("--------------------4");
    console.log(PassWordSet);
    if(find = true){
      text = "password เคยใช้งานแล้ว"
    }else{
      text = "password ไม่ถูกต้อง"
    }
  }
  if(Station == false){
    await client.db('mydb').collection('users').updateOne({'userName': userName}, {"$set": {
      userName: user.userName,
      passWord:PassWordSet,
      fname: user.fname,
      lname: user.lname,
      img: user.img
    }});
    await client.close();
    res.status(200).send({
      "status": true,
      "message": "pass",
      "user": user
    });
  }else{
    await client.close();
    res.status(200).send({
      "status": false,
      "message": text,
    });
  }


})

app.post('/users/userFind', async(req, res) => {
  const user = req.body;
  const userName = user.userName;
  const client = new MongoClient(uri);
  check = false
  await client.connect();
  const userFind = await client.db('mydb').collection('users').findOne({"userName": userName});
  console.log(userFind);
  if(userFind == null){
    await client.close();
    res.status(200).send({
      "status": false
    });
    return
  }
  if(userFind.userName == user.userName && userFind.passWord[userFind.passWord.length-1] == user.passWord){
    await client.close();
    res.status(200).send({
      "status": true,
      "user": userFind
    });
  }else{
    console.log(userFind.passWord[3]);
    console.log(user.passWord);
    await client.close();
    res.status(200).send({
      "status": false
    });
  }



})

app.post('/users/delete', async(req, res) => {
  const id = parseInt(req.body.id);
  const client = new MongoClient(uri);
  await client.connect();
  await client.db('mydb').collection('users').deleteOne({'id': id});
  await client.close();
  res.status(200).send({
    "status": "ok",
    "message": "User with ID = "+id+" is deleted"
  });
})